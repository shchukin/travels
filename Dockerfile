FROM golang

RUN apt-get update && apt-get install zip unzip

ENV PATH=${PATH}:/usr/local/go/bin GOROOT=/usr/local/go GOPATH=/go DOCKER_RUN=yes

RUN mkdir -p /go/src/github.com/alex-shch/travels

ADD . /go/src/github.com/alex-shch/travels

RUN go build github.com/alex-shch/travels && go install github.com/alex-shch/travels

EXPOSE 80

CMD mkdir data && unzip /tmp/data/data.zip -d data/ >> /dev/null && /go/bin/travels

